package com.inacap.recetas;

        import androidx.appcompat.app.AppCompatActivity;

        import android.os.Bundle;
        import android.text.method.ScrollingMovementMethod;
        import android.view.View;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.TextView;

        import com.android.volley.Request;
        import com.android.volley.RequestQueue;
        import com.android.volley.Response;
        import com.android.volley.VolleyError;
        import com.android.volley.toolbox.StringRequest;
        import com.android.volley.toolbox.Volley;

        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;


public class MainActivity extends AppCompatActivity {

    private EditText et_nombre;
    private TextView txt_ingrediente,txt_calorias,textnomreceta;
    private Button bt_buscar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.txt_ingrediente = (TextView) findViewById(R.id.txt_ingrediente);
        this.textnomreceta = (TextView) findViewById(R.id.textnomreceta);
        this.txt_calorias = (TextView) findViewById(R.id.txt_calorias);
        this.et_nombre = (EditText) findViewById(R.id.et_nombre);
        this.bt_buscar = (Button) findViewById(R.id.bt_buscar);


        bt_buscar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                final String nombre = et_nombre.getText().toString().trim();


                String url = "http://api.edamam.com/search?q="+nombre+"&app_id=0666479b&app_key=ffd77b4cb145b143194f49c087a96679";
                StringRequest solicitud = new StringRequest(
                        Request.Method.GET,
                        url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // Respuesta
                                try {
                                    JSONObject respuestaJSON = new JSONObject(response);

                                    JSONArray hitsJSON = respuestaJSON.getJSONArray("hits");
                                    JSONObject primer = hitsJSON.getJSONObject(0);
                                    JSONObject recipesJSON  = primer.getJSONObject("recipe");
                                    String NombreReceta = recipesJSON.getString("label");
                                    textnomreceta.setText(NombreReceta);
                                    JSONArray in = recipesJSON.getJSONArray("ingredientLines");
                                    String ingre = "" ;

                                    for(int i = 0; i<in.length();i++){

                                        ingre = ingre + in.getString(i)+ "\n" ;


                                    }
                                    txt_ingrediente.setText(ingre);


                                    JSONObject nutJSON = recipesJSON.getJSONObject("totalNutrients");
                                    JSONObject enerJSON =nutJSON.getJSONObject("ENERC_KCAL");
                                    String enerlabel = enerJSON.getString("label");
                                    String enerq = enerJSON.getString("quantity");
                                    String eneru = enerJSON.getString("unit");

                                    JSONObject fatJSON =nutJSON.getJSONObject("FAT");
                                    String fatlabel =fatJSON.getString("label");
                                    String fatq = fatJSON.getString("quantity");
                                    String fatu = fatJSON.getString("unit");

                                    JSONObject fasatJSON =nutJSON.getJSONObject("FASAT");
                                    String fasatlabel = fasatJSON.getString("label");
                                    String fasatq = fasatJSON.getString("quantity");
                                    String fasatu = fasatJSON.getString("unit");


                                    String todo = enerlabel + " = " + enerq + " - " + eneru + "\n" +
                                            fatlabel + " = " + fatq + " - " + fatu + "\n" +
                                            fasatlabel + " = " + fasatq + " - " + fasatu + "\n" ;

                                    txt_calorias.setText(todo);






                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }
                );

                RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
                listaEspera.add(solicitud);

            }
        });


    }
}
